#!/bin/bash

macaddr=`cat /sys/class/net/eno1/address`
listofmacaddr=("00:1f:c6:9c:ef:73" "00:1f:c6:9c:ef:72" "00:1f:c6:9c:ef:71")

for i in "${listofmacaddr[@]}"
do
  if [ $i = $macaddr ]
  then
    echo "lte" | sudo -S echo -n 2>/dev/random 1>/dev/random
    sudo rm /home/lte/hss.log &> /dev/null
    sudo rm /home/lte/mme.log &> /dev/null
    sudo rm /home/lte/spgw.log &> /dev/null
    sudo rm /home/lte/enb.log &> /dev/null
    sudo killall oai_hss &> /dev/null
    sudo killall mme &> /dev/null
    sudo killall spgw &> /dev/null
    sudo killall lte-softmodem.Rel14 &> /dev/null
    echo "Start HSS"
    sudo /usr/local/bin/oai_hss 2>&1 > /home/lte/hss.log &
    sleep 10
    echo "Start MME"
    sudo /usr/local/bin/mme 2>&1 > /home/lte/mme.log &
    sleep 10
    echo "Start SPGW"
    sudo /usr/local/bin/spgw 2>&1 > /home/lte/spgw.log &
    sleep 10
    echo "Start eNB"
    sudo /usr/local/bin/lte-softmodem.Rel14 -O /home/lte/enb-limesdr/enb.band7.tm1.25PRB.lmssdr.conf --rf-config-file /home/lte/enb-limesdr/LimeSDR_above_1p8GHz_1v4.ini -d 2>&1 > /home/lte/enb.log &
  else
    echo "You have no rights to execute this script. Sorry!"
  fi
done
