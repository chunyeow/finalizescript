#!/bin/bash

sudo cp ~/openairinterface5g/targets/bin/lte-softmodem.Rel14 /usr/local/bin
sudo cp ~/openairinterface5g/targets/bin/liboai_lmssdrdevif.so.Rel14 /usr/local/lib
cd /usr/local/lib
sudo ln -s liboai_lmssdrdevif.so.Rel14 liboai_device.so
sudo ldconfig
rm -rf ~/openairinterface5g
rm -rf ~/openair-cn
rm -rf ~/LimeSuite
rm -rf ~/SoapySDR
rm -rf ~/epc.tar.gz
rm -rf ~/epc
rm -rf ~/enb-limesdr.tar.gz
