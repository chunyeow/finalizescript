#!/bin/bash

echo "lte" | sudo -S echo -n 2>/dev/random 1>/dev/random
sudo rm /home/lte/hss.log &> /dev/null
sudo rm /home/lte/mme.log &> /dev/null
sudo rm /home/lte/spgw.log &> /dev/null
sudo rm /home/lte/enb.log &> /dev/null
sudo killall oai_hss &> /dev/null
sudo killall mme &> /dev/null
sudo killall spgw &> /dev/null
sudo killall lte-softmodem.Rel14 &> /dev/null
